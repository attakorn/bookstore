package com.attakorn.bookstore.orders;

import com.attakorn.bookstore.books.BookDataService;
import com.attakorn.bookstore.model.Book;
import com.attakorn.bookstore.model.Order;
import com.attakorn.bookstore.model.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * Order controller
 */
@RestController
public class OrderController {

    @Autowired
    private OrderRepository repository;

    @Autowired
    private BookDataService bookDataService;

    /**
     * Order books and store order information in Orders table (DB). This returns the price for a
     * successful order.
     * @param order Order
     * @return Price
     */
    @PostMapping("/users/orders")
    public ResponseEntity<Quote> placeOrder(@RequestBody Order order){

        int[] items = order.getItems();
        if ((items == null) || (items.length == 0)) {
            return new ResponseEntity<>(new Quote(0), HttpStatus.OK);
        }

        // Set order Id
        order.setId(UUID.randomUUID().toString());

        // Get username
        String username = SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();

        // Set current username to order
        order.setUsername(username);

        // Save to repository
        repository.save(order);

        // Calculate total amount of ordered books
        int totalAmount = 0;
        for (int bookId : order.getItems()) {
            Book book = bookDataService.getBooksDictionary().get(bookId);
            totalAmount += book.getPrice();
        }

        Quote quote = new Quote(totalAmount);
        return new ResponseEntity<>(quote, HttpStatus.OK);
    }
}
