package com.attakorn.bookstore.orders;

import com.attakorn.bookstore.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Repository for order
 * Use Mongo DB as the storage
 */
public interface OrderRepository extends MongoRepository<Order, String> {
    Order[] findByUsername(String username);
}
