package com.attakorn.bookstore;

import com.attakorn.bookstore.books.BookDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class BookstoreApplication implements CommandLineRunner {

	@Autowired
	private BookDataService bookDataService;

	public static void main(String[] args) {
		SpringApplication.run(BookstoreApplication.class, args);
	}

    @Override
    public void run(String... args) throws Exception {
		// Call book service at startup to load and cache all book data
		// in advance before incoming API call.
		try {
			this.bookDataService.getBooks();
			this.bookDataService.getBooksDictionary();
			this.bookDataService.getRecommendedBooks();
		}
		catch (Exception ex) {
			// Do nothing for now
		}
    }
}
