package com.attakorn.bookstore.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model class for price quote
 */
public class Quote {

    // =================================================
    // Private Fields
    // =================================================

    @JsonProperty("price")
    private int price;

    // =================================================
    // Constructors
    // =================================================

    /**
     * Constructor
     * @param price Price
     */
    public Quote(int price) {
        this.setPrice(price);
    }

    // =================================================
    // Getters / Setters
    // =================================================

    /**
     * Get price
     * @return Price
     */
    public int getPrice() {
        return price;
    }

    /**
     * Set price
     * @param price Price
     */
    public void setPrice(int price) {
        this.price = price;
    }
}
