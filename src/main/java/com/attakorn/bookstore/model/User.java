package com.attakorn.bookstore.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * Model class for user
 */
public class User {

    // =================================================
    // Private Fields
    // =================================================

    @Id
    @JsonProperty("username")
    private String username;

    @JsonProperty("password")
    private String password;

    @JsonProperty("firstname")
    private String firstname;

    @JsonProperty("lastname")
    private String lastname;

    // We expect the date pattern to be "dd MMM yyyy", for example, 1 Jan 2018.
    @JsonProperty("date_of_birth")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd MMM yyyy")
    private Date dateOfBirth;

    // =================================================
    // Constructors
    // =================================================

    /**
     * Constructor
     */
    public User() {}

    /**
     * Constructor
     * @param username Username
     * @param password Password
     * @param firstname First name
     * @param lastname Last name
     * @param dateOfBirth Date of birth
     */
    public User(String username, String password, String firstname, String lastname, Date dateOfBirth) {
        this.setUsername(username);
        this.setPassword(password);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setDateOfBirth(dateOfBirth);
    }

    // =================================================
    // Getters / Setters
    // =================================================

    /**
     * Get username
     * @return Username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set username
     * @param username Username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get password
     * @return Password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set password
     * @param password Password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get first name
     * @return First name
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Set first name
     * @param firstname First name
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Get last name
     * @return Last name
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Set last name
     * @param lastname Last name
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * Get date of birth
     * @return Date of birth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Set date of birth
     * @param dateOfBirth Date of birth
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
