package com.attakorn.bookstore.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model class for book
 */
public class Book {

    // =================================================
    // Private Fields
    // =================================================

    @JsonProperty("id")
    private int id;

    private String name;

    private String author;

    @JsonProperty("price")
    private int price;

    @JsonProperty("is_recommended")
    private boolean isRecommended = false;

    // =================================================
    // Getters / Setters
    // =================================================

    /**
     * Get book ID
     * @return Book ID
     */
    public int getId() {
        return id;
    }

    /**
     * Set book ID
     * @param id Book ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Set book name
     * @param name Book name
     */
    @JsonProperty("book_name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get book name
     * @return Book name
     */
    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    /**
     * Set author name
     * @param author Author name
     */
    @JsonProperty("author_name")
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Get author name
     * @return Author name
     */
    @JsonProperty("author")
    public String getAuthor() {
        return this.author;
    }

    /**
     * Get price
     * @return Price
     */
    public int getPrice() {
        return price;
    }

    /**
     * Set price
     * @param price Price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * Get recommendation flag
     * @return True if this book is recommended, otherwise False
     */
    @JsonIgnore
    public boolean isRecommended() {
        return isRecommended;
    }

    /**
     * Set recommendation flag
     * @param recommended True if this book is recommended, otherwise False
     */
    public void setRecommended(boolean recommended) {
        isRecommended = recommended;
    }
}
