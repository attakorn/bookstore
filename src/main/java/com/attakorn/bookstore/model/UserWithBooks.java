package com.attakorn.bookstore.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Model class for user with orders
 */
public class UserWithBooks {

    // =================================================
    // Private Fields
    // =================================================

    @JsonProperty("firstname")
    private String firstname;

    @JsonProperty("lastname")
    private String lastname;

    // We expect the date pattern to be "dd MMM yyyy", for example, 1 Jan 2018.
    @JsonProperty("date_of_birth")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd MMM yyyy")
    private Date dateOfBirth;

    @JsonProperty("books")
    private int[] books;

    // =================================================
    // Constructors
    // =================================================

    /**
     * Constructor
     * @param firstname First name
     * @param lastname Last name
     * @param dateOfBirth Date of birth
     * @param books Books
     */
    public UserWithBooks(String firstname, String lastname, Date dateOfBirth, int[] books) {
        setFirstname(firstname);
        setLastname(lastname);
        setDateOfBirth(dateOfBirth);
        setBooks(books);
    }

    /**
     * Constructor
     * @param user User
     * @param books Books
     */
    public UserWithBooks(User user, int[] books) {
        this(user.getFirstname(), user.getLastname(), user.getDateOfBirth(), books);
    }

    // =================================================
    // Getters / Setters
    // =================================================

    /**
     * Get first name
     * @return First name
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Set first name
     * @param firstname First name
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Get last name
     * @return Last name
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Set last name
     * @param lastname Last name
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * Get date of birth
     * @return Date of birth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Set date of birth
     * @param dateOfBirth Date of birth
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Get books
     * @return Books
     */
    public int[] getBooks() {
        return books;
    }

    /**
     * Set books
     * @param books Books
     */
    public void setBooks(int[] books) {
        this.books = books;
    }
}
