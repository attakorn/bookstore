package com.attakorn.bookstore.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

/**
 * Model class for order
 */
public class Order {

    // =================================================
    // Private Fields
    // =================================================

    @Id
    @JsonProperty("id")
    private String id;

    @JsonProperty("username")
    private String username;

    @JsonProperty("orders")
    private int[] items;

    // =================================================
    // Getters / Setters
    // =================================================

    /**
     * Get Id
     * @return Id
     */
    public String getId() {
        return id;
    }

    /**
     * Set Id
     * @param id Id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get username
     * @return Username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set username
     * @param username Username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get order items
     * @return Order items
     */
    public int[] getItems() {
        return items;
    }

    /**
     * Set order items
     * @param items Order items
     */
    public void setItems(int[] items) {
        this.items = items;
    }
}
