package com.attakorn.bookstore.books;

import com.attakorn.bookstore.model.Book;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * Service to get book data from 3rd party REST API
 */
@Service
public class BookDataService {

    /**
     * Get books
     * @return Books
     */
    @Cacheable("books")
    public Book[] getBooks() {
        // Get books from SCB books service
        RestTemplate bookRestTemplate = new RestTemplate();
        ResponseEntity<Book[]> bookResponseEntity = bookRestTemplate.getForEntity("https://scb-test-book-publisher.herokuapp.com/books", Book[].class);
        return bookResponseEntity.getBody();
    }

    /**
     * Get recommended books
     * @return Recommended books
     */
    @Cacheable("recommended-books")
    public Book[] getRecommendedBooks() {
        // Get books from SCB recommended books service
        RestTemplate recommendedBookRestTemplate = new RestTemplate();
        ResponseEntity<Book[]> recommendedBookResponseEntity = recommendedBookRestTemplate.getForEntity("https://scb-test-book-publisher.herokuapp.com/books/recommendation", Book[].class);
        return recommendedBookResponseEntity.getBody();
    }

    /**
     * Get books dictionary
     * @return Books dictionary
     */
    @Cacheable("books-dict")
    public Map<Integer, Book> getBooksDictionary() {
        Map<Integer, Book> map = new HashMap<Integer, Book>();
        Book[] books = this.getBooks();

        // Add to map
        for (Book book : books) {
            map.put(book.getId(), book);
        }

        return map;
    }
}
