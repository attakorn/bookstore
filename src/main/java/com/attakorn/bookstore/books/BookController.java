package com.attakorn.bookstore.books;

import com.attakorn.bookstore.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static java.util.Comparator.reverseOrder;

/**
 * Book controller
 */
@RestController
public class BookController {

    @Autowired
    private BookDataService bookDataService;

    /**
     * Gets a list of books from an external book publisher’s web services and returns the
     * list sorted alphabetically with the recommended books always appears first. There should be no
     * duplicated books in the list.
     * @return List of books
     */
    @RequestMapping("/books")
    public ResponseEntity<List<Book>> getAllBooks()
    {
        // Get books
        Book[] books = this.bookDataService.getBooks();
        Book[] recommendedBooks = this.bookDataService.getRecommendedBooks();

        // Set recommendation flag to books according to recommended books data
        for (Book book : books) {
            if (Arrays.stream(recommendedBooks).anyMatch(x -> x.getId() == book.getId())) {
                book.setRecommended(true);
            }
        }

        // Order books by recommendation flag and book name
        Comparator<Book> bookComparator
                = Comparator.comparing((Book x) -> x.isRecommended(), reverseOrder()).thenComparing(x -> x.getName());

        Arrays.sort(books, bookComparator);

        return new ResponseEntity<>(Arrays.asList(books), HttpStatus.OK);
    }
}
