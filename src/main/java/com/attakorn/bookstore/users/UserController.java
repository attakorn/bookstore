package com.attakorn.bookstore.users;

import com.attakorn.bookstore.model.Order;
import com.attakorn.bookstore.model.User;
import com.attakorn.bookstore.model.UserWithBooks;
import com.attakorn.bookstore.orders.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;

/**
 * User controller
 */
@RestController
public class UserController {

    @Autowired
    private UserRepository repository;

    @Autowired
    private OrderRepository orderRepository;

    /**
     * Get all users
     * @return All users
     */
    @RequestMapping(value = "/users/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> getAllUsers() {
        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
    }

    /**
     * Gets information about the logged in user. A successfully authenticated request returns
     * information related to the user and the books ordered.
     * @param principal Principal
     * @return User and books ordered
     */
    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserWithBooks> getUser(Principal principal) {
        User user = repository.findByUsername(principal.getName());
        Order[] orders = orderRepository.findByUsername(principal.getName());
        List<Integer> bookList = new ArrayList<>();

        // Add all purchased books for current user
        for (Order order : orders) {
            for (int item : order.getItems()) {
                bookList.add(item);
            }
        }

        // Sort books
        Collections.sort(bookList);

        // Convert list of Interger to array of int
        int[] books = bookList.stream().mapToInt(i->i).toArray();

        // Create model for user with their books
        UserWithBooks userWithBooks = new UserWithBooks(user, books);

        return new ResponseEntity<>(userWithBooks, HttpStatus.OK);
    }

    /**
     * Create a user account and store user’s information in Users table (DB).
     * @param user User
     * @return User's information
     */
    @PostMapping("/users")
    public ResponseEntity<String> createUser(@RequestBody User user){

        // Encode password before saving to DB
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        repository.save(user);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Delete logged in user’s record and order history.
     */
    @RequestMapping(value = "/users", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteUser(Principal principal) {
        // Delete orders
        Order[] orders = orderRepository.findByUsername(principal.getName());
        for (Order order : orders) {
            orderRepository.delete(order);
        }

        // Delete user
        User user = repository.findByUsername(principal.getName());
        repository.delete(user);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
