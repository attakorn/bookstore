package com.attakorn.bookstore.users;

import com.attakorn.bookstore.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Repository for user
 * Use Mongo DB as the storage
 */
public interface UserRepository extends MongoRepository<User, String> {
    User findByUsername(String username);
}