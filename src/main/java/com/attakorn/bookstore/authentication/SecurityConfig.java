package com.attakorn.bookstore.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Configuration class for Spring Security
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private BookstoreAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private BookstoreAuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    private BookstoreAuthenticationSuccessHandler authenticationSuccessHandler;

    /**
     * Get our own implementation of user details service
     * @return User details service
     */
    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    };

    /**
     * Get the password encoder used in Bookstore project
     * @return Password encoder
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    };

    /**
     * Get our own UsernamePasswordAuthenticationFilter derivative
     * @return UsernamePasswordAuthenticationFilter derivative
     * @throws Exception
     */
    @Bean
    public JsonUsernamePasswordAuthenticationFilter jsonUsernamePasswordAuthenticationFilter()
            throws Exception {

        JsonUsernamePasswordAuthenticationFilter authenticationFilter = new JsonUsernamePasswordAuthenticationFilter();
        authenticationFilter.setAuthenticationManager(authenticationManagerBean());
        authenticationFilter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
        authenticationFilter.setAuthenticationFailureHandler(authenticationFailureHandler);
        authenticationFilter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login", "POST"));

        return authenticationFilter;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // Use our own user details service instead of the default one
        // since we need to use our own database to store our user credentials
        // and use the specific password encoder
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()

                // According to assignment specification, API to get books and create a new user
                // doesn't require user to login
                .antMatchers(HttpMethod.GET, "/books").permitAll()
                .antMatchers(HttpMethod.POST, "/users").permitAll()
                .antMatchers(HttpMethod.GET, "/users/all").permitAll()

                // For other API, we need user to login first
                .anyRequest().authenticated();

        // Add custom UsernamePasswordAuthenticationFilter to support login credential in JSON data
        http.addFilterBefore(jsonUsernamePasswordAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

        // Tells Spring Security what happens when an unauthenticated client tries to access a restricted resource.
        // The service should return a 401 HTTP status code. A custom authentication entry point is used to implement
        // that behavior
        http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
    }
}
